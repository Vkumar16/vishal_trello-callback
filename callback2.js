/* 
    Problem 2: 
    1.Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. 
    2.Then pass control back to the code that called it by using a callback function.
*/
const lists = require('./data/lists.json')

function allListsThatBelongToBoard(cb, boardID) {
    setTimeout(() => {
        if (boardID !== undefined) {

            if (boardID in lists) {
                cb(null, lists[boardID]);
            } else {
                cb(new Error("Data Not Found"));
            }
        } else {
            cb(new Error("Something went wrong!"));
        }
    }, 2 * 1000);

}

module.exports = allListsThatBelongToBoard;