/* 
    Problem 5: Write a function that will use the previously written functions to get the following information.
     You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const boardInformation = require('./callback1');
const allListsThatBelongToBoard = require('./callback2');
const allCardsBelongToParticularList = require('./callback3');
const boards = require('./data/boards.json');
const cards = require('./data/cards.json');
const lists = require('./data/lists.json');


function getBoardID(boardName) {
    let board;
    if(boardName !== undefined)
   {
    board = boards.find((board) =>
    board.name == boardName);
   }
   if(board)
    {
        return board.id;
    }
    else{
        return "";
    }

}

function getListID(boardName, ...listName) {

    boardID = getBoardID(boardName);
    let listData;
  
    for (let list in lists) {
        if (list === boardID) {
            listData = lists[list];
            break;
        }
    }
    let listIDs = listData.filter((eachList) => listName.indexOf(eachList.name) != -1)
                                 .reduce((accum,eachList) => {
                                     accum.push(eachList.id);
                                     return accum;
                                 },[]);
    if(Array.isArray(listIDs))
    {
        return listIDs;
    }else{
        return "";
    }
}


function callback5(boardName, ...nameList) {

let boardID = getBoardID(boardName);
let listIDs = getListID(boardName,...nameList);


    boardInformation((error, data) => {
        if (error) {
            console.log(error);
        } else {
            console.log(`board's information of boardID ${boardID} : \n`, data);
        }
    }, boardID);


    allListsThatBelongToBoard((error, data) => {
        if (error) {
            console.log(error);
        } else {
            console.log(`all lists that belong to a board with the boardID  ${boardID} : \n `, data);
        }

    }, boardID);


    allCardsBelongToParticularList((error, data) => {
        if (error) {
            console.log(error);
        } else {
            console.log(`all cards that belong to listID  : \n `, data);
        }
    }, ...listIDs)

}
module.exports = callback5;