/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID 
    that is passed from the given list of boards in boards.json 
    and then pass control back to the code that called it by using a callback function.
*/

const boards = require('./data/boards.json');

function boardInformation(cb, boardID) {
    setTimeout(() => {
        if (boardID !== undefined) {
            const data = boards.find((board) => board.id == boardID);
            if (data) {
                cb(null, data);
            } else {
                cb(new Error("Data not found!"))
            }

        }
        else {
            cb(new Error("Something went wrong!"));
        }

    }, 2 * 1000)

}
module.exports = boardInformation;
