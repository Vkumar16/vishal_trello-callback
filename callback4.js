/* 
    Problem 4: Write a function that will use the previously written functions to get the following information.
               You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const boardInformation = require('./callback1');
const allListsThatBelongToBoard = require('./callback2');
const allCardsBelongToParticularList = require('./callback3');
const boards = require('./data/boards.json');
const cards = require('./data/cards.json');
const lists = require('./data/lists.json');

function getBoardID(boardName) {
    board = boards.find((board) =>
        board.name == boardName);
    return board.id;

}

function getListID(boardName, listName) {

    boardID = getBoardID(boardName);
    let listData;

    for (let list in lists) {
        if (list === boardID) {
            listData = lists[list];
            break;
        }
    }

    let listnameData;
    if (Array.isArray(listData)) {
        listnameData = listData.find((eachList) => {
            if (eachList.name == listName) {
                return eachList;
            }
        })
    }

    if (listnameData) {
        return listnameData.id;
    }
    else {
        return "";
    }
}

function callback4(boardName, listName) {
    let boardID = getBoardID(boardName);
    let listID = getListID(boardName, listName);

    boardInformation((error, data) => {
        if (error) {
            console.log(error);
        } else {
            console.log(`board's information of boardID ${boardID} : \n`, data);
        }
    }, boardID);


    allListsThatBelongToBoard((error, data) => {
        if (error) {
            console.log(error);
        } else {
            console.log(`all lists that belong to a board with the boardID  ${boardID} : \n `, data);
        }

    }, boardID);


    allCardsBelongToParticularList((error, data) => {
        if (error) {
            console.log(error);
        } else {
            console.log(`all cards that belong to listID of ${listID} : \n `, data);
        }
    }, listID)

}
module.exports = callback4;