/* 
    Problem 3:
    1. Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. 
    2.Then pass control back to the code that called it by using a callback function.
*/

const cards = require('./data/cards.json');

function allCardsBelongToParticularList(cb, ...listsID) {
    setTimeout(() => {
        
        if (listsID !== undefined) {
            listsID.forEach((listID) => {
                if (listID in cards) {
                    cb(null, cards[listID]);
                } else {
                    cb(new Error("Data not found for id: "+ listID));
                }
            })


        } else {
            cb(new Error("Something went wrong!"))
        }
    }, 2 * 1000);
}

module.exports = allCardsBelongToParticularList;