const allCardsBelongToParticularList = require('../callback3');

let listID = "qwsa221";

allCardsBelongToParticularList((error, data) => {

    if (error) {
        console.log(error);
    } else {
        console.log(`all cards that belong to listID of ${listID} : \n `, data);
    }
    
}, listID)
