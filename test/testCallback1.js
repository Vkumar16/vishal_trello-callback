const boardInformation = require('../callback1');

const boardID = 'abc122dc';

boardInformation((error, data) => {
    if (error) {
        console.log(error);
    } else {
        console.log(`board's information of boardID ${boardID} : \n`, data);
    }
}, boardID);