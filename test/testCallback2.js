const allListsThatBelongToBoard = require('../callback2');

const boardID = "abc122dc";

allListsThatBelongToBoard((error, data) => {
    if (error) {
        console.log(error);
    } else {
        console.log(`all lists that belong to a board with the boardID  ${boardID} : \n`, data);
    }

}, boardID);

